// JS ES6 Updates
// ECMAScript - the technology that is used to create the languages such as JavaScript

// exponent operator
	// pre-es6
	const firstNum = Math.pow(8,2);
	console.log(firstNum);

	// es6
	const secondNum = 8**2;
	console.log(secondNum);


// Template literals - allows writing of strings without the use of concat operator; helps in terms of readability of the codes as well as the efficieny of work

// pre-es6
let name = "John"	
let message = "Hello " + name + "! Welcome to the programming field."
console.log(message)

// es6
message = `Hello ${name}! Welcome in the Programming Field.`
console.log(message)

// Multiline
const anotherMessage = `
${name} attended a math competition. 
He won it by solving the problem 8**2 with answer of ${secondNum}
`

console.log(anotherMessage)

// computation inside the template literals
const interestRate = .10;
const principal = 1000;
console.log(`The interest in your savings is ${principal * interestRate}`);


// Array Destructuring - allows unpacking elements in arrays into distinct vairiables; allows naming of array elements with variable instead of using index numbers; helps with the code readability and coding efficiency
/*
	SYNTAX:
		let/const [variableA, variableB, variableC, .... variableN]
*/

// pre-es6
const fullName = ["Juan", "Dela", "Cruz"]
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);


// es6
const [firstName, middleName, lastName] = fullName
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}!`)

// Object Destructuring - allows unpacking 

// pre- es6
let woman = {
	givenName:"Jane",
	maidenName: "Dela",
	familyName: "Cruz",
}
console.log(woman.givenName);
console.log(woman.maidenName);
console.log(woman.familyName);
console.log(`Hello ${woman.givenName} ${woman.maidenName} ${woman.familyName}!`)

// es6
// the key and the variable should match
const {givenName, maidenName, familyName } = woman;
console.log(woman.givenName);
console.log(woman.maidenName);
console.log(woman.familyName);
console.log(`Hello ${woman.givenName} ${woman.maidenName} ${woman.familyName}!`)


// Arrow Function
/*
	compact alternative syntax to traditional functions; useful for code snippets where creating functions will not be reused in any other parts of the code. Don't Repeat Yourself - there is no need to create a function that will not be used in the other parts/portions of the code
*/


// pre-es6
/*
Parts
	declaration
	function name
	parameters
	statements
	invoke/call the function
*/

function printFullName(firstName, middleInitial, lastName){
/*	console.log(firstName);
	console.log(middleInitial);
	console.log(lastName);*/
	console.log(firstName, middleInitial, lastName );
};

printFullName("Jane","Reyes","Rivera");

// es6
const printFName=(fname, mname, lname) => {
	console.log(fname, mname, lname);
}
printFName("Will", "D.", "Smith");

// mini activity
//arrows functions with loop/methods
// pre es6
const students = [`John`, `Jane`, `Joe`]


students.forEach(
		function(stud){
			console.log(stud)}
	)


// (Arrow Functions) Implicit Return Statement - return statement/s are omitted because even without them, JS implicitly adds them for the result of the function
// es6
// if you have 2 or more parameters, enclose them inside a pair of parenthesis
students.forEach(x => console.log(x))

// mini activity

// pre-es6
function addNumbers(x,y) {
	return x+y
}

let total = addNumbers(1,2)
console.log(total)

// es6
const adds=(x,y) => x+y;
//  the code above actually runs as const add = (x,y) => return x+y;
let totals=adds(4,6);
console.log(totals);

// (Arrow Function) Default Function Argument Value
/*
	provides a default argument value if no parameters are included/specified once the function has been invoked
*/

const greet = (name="User") => {
	return `Good morning, ${name}`
}
console.log(greet())
// once the function has specified parameter value
console.log(`Result of specified value for parameter: ${greet("John")}`)

// mini acticity

/*const car = {
	name: "city",
	brand: "honda",
	year: 2021,
}

console.log(car)*/

/*function car(name, brand, year){
	this.name = name
	this.brand = brand
	this.year = year
};

const car1 = new car("Honda", "Vios", 2020)
console.log(car1)

const car2 = new car("Honda", "City", 2020)
console.log(car2)*/

// class constructor
// class keyword declares the creation of a "car" object
class car{
	// constructor keyword - special method of creating/initializing an object for the "car"
	constructor(brand, name, year){
		// this - sets the properties that included to the "car" class
		this.brand = brand,
		this.name = name,
		this.year = year
	}
};
const car1 = new car("Ford", "Ranger Raptor", 2021)
console.log(car1)

const car2 = new car()
console.log(car2)
car2.brand = "Toyota",
car2.name = "Fortuner",
car2.year = 2020
console.log(car2)

// mini activity


let num = 10;
/*if (num <=0){
	console.log(true)
}else {
	console.log(false)
}
*/

(num <= 0) ? console.log(true): console.log(false);