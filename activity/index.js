const getCube = 2**3;
let message = `The cube of 2 is ${getCube}`
console.log(message)

const address = ["258 Washington Ave NW,", "California", "90011",]

let [street, state, zipCode] = address
// const {street, state, zipCode } = address;
console.log(`I live at ${street} ${state} ${zipCode}`)


const animal = 
[	"Lolong",
	 "salt water crocodile",
	 1075,
	 20,  
	 3,
]

let [name, type, weight, ft, inch] = animal


console.log(`${name} was a ${type}. He weighed ${weight} kgs with a measurement of ${ft} ft ${inch} in`)


let numbers = []

numbers[0] = 1;
numbers[1] = 2;
numbers[2] = 3;
numbers[3] = 4;
numbers[4] = 5;

numbers.forEach(x => console.log(x))

let findSum = (a,b,c,d,e) => a+b+c+d+e
let reduceNumber=findSum(1,2,3,4,5)
console.log(reduceNumber)


class Dog{constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
};
const Dog1 = new Dog("Rocket", 2, "Shitzu")
console.log(Dog1)